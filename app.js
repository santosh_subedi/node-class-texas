var express = require('express');
var path = require('path');
// var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const { ConnectToDatabase } = require('./model');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
ConnectToDatabase()
.then(()=>{
    console.log("COnnected to Database")
})
.catch((error)=>{
    console.log(error)
})
app.use('/', indexRouter);
app.use('/users', usersRouter);

module.exports = app;
