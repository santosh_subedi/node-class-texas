const jwt = require("jsonwebtoken");
const validateToken=()=>{
    return (req,res,next)=>{
        const bearer = req.headers['authorization'];
        if(!bearer){
            res.status(401);
            res.json({message:"unauthorized access"})
        }else{
            const tokenSet = bearer.split(" ");
            const token = tokenSet[1];
            const verified = jwt.verify(token,"secret");
            if(verified){
                req.user = {email:verified.username,id:verified.userId}
                next()
            }else{
            res.status(401);
            res.json({message:"unauthorized access"}) 
            }
        }
    }
}

module.exports={validateToken}