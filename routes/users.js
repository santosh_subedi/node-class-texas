var express = require('express');
var router = express.Router();
const jwt = require("jsonwebtoken");
const { User, Role, UserRoleMapping } = require('../model');
const { validateToken } = require('../middleware/auth-middleware');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post("/login",async (req,res,next)=>{
  const user = await User.findOne({email:req.body.username});  
  if(req.body.username == user.email& req.body.password==user.password){
    const roles = await UserRoleMapping.findAll({where:{user_id:user.id}});
    const userRoles = [];
    for (const role of roles) {
      const roleEnt = await Role.findOne({where:{id:role.role_id}});
      userRoles.push(roleEnt.code);
    }
    const token = jwt.sign({
          username:req.body.username,
          userId:user.id,
          name:user.name,
          roles: userRoles
        },"secret",{expiresIn:"2h"})
        res.json({token})
      }else{
        res.json({message:"Username Password didn't match"})
      }
  });

  router.post("/register",async (req,res,next)=>{
    const user = await User.create(req.body);
    const role = await Role.findOne({where:{code:"USER"}});
    await UserRoleMapping.create({user_id:user.id,role_id:role.id});
    res.json({message:"User Registered Successfully"})
  })

  router.get("/userinfo",validateToken(),async(req,res,next)=>{
    const userid = req.user.id;
    const user = await User.findOne({where:{id:userid},attributes: {exclude: ['password']}});
    return res.json(user);
  })

module.exports = router;
